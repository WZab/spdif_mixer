# spdif_mixer

Mixer for combining multiple SPDIF streams with slightly different clock frequencies.
The output should be a single SPDIF stream with the clock frequency taken from one of input streams or received from and external source.