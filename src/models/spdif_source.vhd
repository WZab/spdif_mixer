-------------------------------------------------------------------------------
-- Title      : spdif_source
-- Project    : Source of the SPDIF encoded signal
-------------------------------------------------------------------------------
-- File	      : spdif_source.vhd
-- Author     : Wojciech M. Zabołotny & Bartosz M. Zabołotny
-- Company    : 
-- Created    : 2021-02-20
-- Platform   :
-- SPDX-License-Identifier: BSD-3-Clause
-- Standard   : VHDL'93
-------------------------------------------------------------------------------
-- Description: This block should output the SPDIF encoded sinusoidal waveform
-------------------------------------------------------------------------------
-- Copyright (c) 2021
-------------------------------------------------------------------------------
-- Revisions  :
-- Date	       Version	Author	Description
-- 2021-02-20  1.0	WZab	Created
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
use ieee.math_complex.all;

library work;

entity spdif_source is
  generic (
    g_spdif_clk_freq : real := 1.0e6;
    g_sin_freq	     : real := 1.2e3
    );
  port (
    dout : out std_logic
    );

end spdif_source;

architecture sim of spdif_source is

  constant c_sig_per : time	 := 1.0 sec / g_sin_freq;
  signal clk	     : std_logic := '0';

begin

  clk <= not clk after c_sig_per;


  -- Now we only output clk
  dout <= clk;

end sim;
