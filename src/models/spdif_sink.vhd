-------------------------------------------------------------------------------
-- Title      : spdif_sink
-- Project    : Sink of the SPDIF encoded signal
-------------------------------------------------------------------------------
-- File       : spdif_sink.vhd
-- Author     : Wojciech M. Zabołotny & Bartosz M. Zabołotny
-- Company    : 
-- Created    : 2021-02-20
-- Platform   :
-- SPDX-License-Identifier: BSD-3-Clause
-- Standard   : VHDL'93
-------------------------------------------------------------------------------
-- Description: This block should convert the SPDIF encoded signal into analog
--              samples and output them to the file.
--              (now it is just a mock-up anmd does nothing!)
-------------------------------------------------------------------------------
-- Copyright (c) 2021
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2021-02-20  1.0      WZab    Created
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
use ieee.math_complex.all;

library work;

entity spdif_sink is
  port (
    din : in std_logic
    );

end spdif_sink;

architecture sim of spdif_sink is

  signal clk  : std_logic := '0';
  
begin

  process(din) is
  begin
    if rising_edge(din) then
      report "din changes" severity note;      
    end if;
  end process;

end sim;
