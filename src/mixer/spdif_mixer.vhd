-------------------------------------------------------------------------------
-- Title      : spdif_mixer
-- Project    : Mixer of the SPDIF encoded signals
-------------------------------------------------------------------------------
-- File	      : spdif_mixer.vhd
-- Author     : Wojciech M. Zabołotny & Bartosz M. Zabołotny
-- Company    : 
-- Created    : 2021-02-20
-- Platform   :
-- SPDX-License-Identifier: BSD-3-Clause
-- Standard   : VHDL'93
-------------------------------------------------------------------------------
-- Description: This block combines two input SPDIF signals and outputs the
--              combined and resampled version
-------------------------------------------------------------------------------
-- Copyright (c) 2021
-------------------------------------------------------------------------------
-- Revisions  :
-- Date	       Version	Author	Description
-- 2021-02-20  1.0	WZab	Created
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_misc.all;
use ieee.math_real.all;
use ieee.math_complex.all;

library work;

entity spdif_mixer is
  generic (
    g_n_inputs : integer := 2
    );
  port (
    din : in std_logic_vector(g_n_inputs-1 downto 0);
    dout : out std_logic
    );

end spdif_mixer;

architecture sim of spdif_mixer is

begin

  dout <= xor_reduce(din);

end sim;
