-------------------------------------------------------------------------------
-- Title      : Testbench for design "spdif_mixer"
-- Project    : 
-------------------------------------------------------------------------------
-- File       : spdif_mixer_tb.vhd
-- Author     : Wojciech Zabołotny  <wzab@wzab>
-- Company    : 
-- Created    : 2021-02-20
-- Last update: 2021-02-20
-- Platform   :
-- SPDX-License-Identifier: BSD-3-Clause
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyright (c) 2021 
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2021-02-20  1.0      wzab	Created
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
library spdif_mixer;
library spdif_models;


-------------------------------------------------------------------------------

entity spdif_mixer_tb is

end entity spdif_mixer_tb;

-------------------------------------------------------------------------------

architecture sim of spdif_mixer_tb is

  -- component generics
  constant g_n_inputs : integer := 2;

  -- component ports
  signal din  : std_logic_vector(g_n_inputs-1 downto 0);
  signal dout : std_logic;

  -- clock
  signal Clk : std_logic := '1';

begin  -- architecture sim

  spdif_source_1: entity spdif_models.spdif_source
    generic map (
      g_spdif_clk_freq => 1.0e6,
      g_sin_freq       => 1.2e3)
    port map (
      dout => din(0));
  
  spdif_source_2: entity spdif_models.spdif_source
    generic map (
      g_spdif_clk_freq => 1.01e6,
      g_sin_freq       => 0.976e3)
    port map (
      dout => din(1));
  
  spdif_mixer_1: entity spdif_mixer.spdif_mixer
    generic map (
      g_n_inputs => g_n_inputs)
    port map (
      din  => din,
      dout => dout);

  spdif_sink_1: entity spdif_models.spdif_sink
    port map (
      din => dout);
  
  -- clock generation
  Clk <= not Clk after 1 ms;

  -- waveform generation
  WaveGen_Proc: process
  begin
    -- insert signal assignments here

    wait until Clk = '1';
  end process WaveGen_Proc;

  

end architecture sim;

-------------------------------------------------------------------------------

configuration spdif_mixer_tb_sim_cfg of spdif_mixer_tb is
  for sim
  end for;
end spdif_mixer_tb_sim_cfg;

-------------------------------------------------------------------------------
