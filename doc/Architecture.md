# Proposed architecture

Proposed algorithm of operation:
- When the new sample arrives, in the input receiver, working at the high clock frequency (frequency of the HR time counter):
    - we record the sample timestamp 
    - we record the value of the sample
    - we write both values to the FIFO
- The input sample processor block (working at lower clock frequency) analyses data from the FIFO _may be written in HLS_?:
    - updates the estimated sampling period for the input
    - adjusts the sample location accordingly
- The output sample processor block (working in the clock domain synchronous with the clock of the output SPDIF interface) calculates the output sample value. _May be written in HLS !_

![test](img/spdif_arch.drawio.svg)
