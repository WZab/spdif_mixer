STD=standard
#STD=synopsys
VSTD=93c
ENTITY=spdif_mixer_tb
# --unbuffered option must be added in the new GHDL
RUN_OPTIONS= --unbuffered --stop-time=5000000ns --wave=./build/${ENTITY}.ghw 
#RUN_OPTIONS+= --trace-signals
#RUN_OPTIONS= --stop-time=290000ns 
#RUN_OPTIONS=  --wave=${ENTITY}.ghw 

# Sources of the models
SOURCES_MOD = \
 src/models/spdif_source.vhd \
 src/models/spdif_sink.vhd \

SOURCES_MIX = \
 src/mixer/spdif_mixer.vhd \

SOURCES_TB = \
 src/tb/spdif_mixer_tb.vhd \

OBJECTS_TB=$(SOURCES_TB:.vhd=.o)
OBJECTS_MIX=$(SOURCES_MIX:.vhd=.o)
OBJECTS_MOD=$(SOURCES_MOD:.vhd=.o)

all: show

$(OBJECTS_TB): %.o : %.vhd
	ghdl -a -g -P./build -C  --workdir=./build --std=${VSTD} --ieee=${STD} $<
$(OBJECTS_MIX): %.o : %.vhd
	ghdl -a -g -P./build --workdir=./build --work=spdif_mixer -C  --std=${VSTD} --ieee=${STD} $<
$(OBJECTS_MOD): %.o : %.vhd
	ghdl -a -g -P./build --workdir=./build --work=spdif_models -C  --std=${VSTD} --ieee=${STD} $<

#--trace-signals --trace-processes
#RUN_OPTIONS= 
#--trace-processes
show:   ./build/${ENTITY} ./build/${ENTITY}.ghw
	gtkwave ./build/${ENTITY}.ghw ./gtkview/${ENTITY}.sav
#	vhdlp -work fmf fmf/*.vhd
./build/${ENTITY}: $(OBJECTS_MIX) $(OBJECTS_MOD) $(OBJECTS_TB)
	ghdl -e -g -P./build -C --workdir=./build  --std=${VSTD} -fexplicit --ieee=${STD} -o ./build/${ENTITY} ${ENTITY}
./build/${ENTITY}.ghw: ./build/${ENTITY}
#	./${ENTITY} --wave=./build/${ENTITY}.ghw  ${RUN_OPTIONS} --stop-time=50000000ns 2>&1 > res.txt
	./build/${ENTITY} ${RUN_OPTIONS} 
#> res.txt  2>&1 
clean:
	rm -rf ./build
	mkdir ./build

